# frozen_string_literal: true

# What anyone who comes to the site sees
class LandingController < ApplicationController
  def index; end
end
