# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'duktape' # See https://github.com/rails/execjs#readme for more supported runtimes
gem 'jbuilder', '~> 2.5' # Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'pg', '>= 0.18', '< 2.0' # Use postgresql as the database for Active Record
gem 'puma', '~> 3.11' # Use Puma as the app server
gem 'rails', '~> 5.2.0' # Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'sass-rails', '~> 5.0' # Use SCSS for stylesheets
gem 'turbolinks', '~> 5' # Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'uglifier', '>= 1.3.0' # Use Uglifier as compressor for JavaScript assets
gem 'webpacker' # Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
# gem 'redis', '~> 4.0' # Use Redis adapter to run Action Cable in production
# gem 'bcrypt', '~> 3.1.7' # Use ActiveModel has_secure_password

# gem 'mini_magick', '~> 4.8' # Use ActiveStorage variant

# gem 'capistrano-rails', group: :development # Use Capistrano for deployment

gem 'bootsnap', '>= 1.1.0', require: false # Reduces boot times through caching; required in config/boot.rb

# SM Gems
gem 'activemodel-serializers-xml'
gem 'devise' # https://github.com/plataformatec/devise
gem 'globalize', github: 'globalize/globalize' # https://github.com/globalize/globalize
gem 'normalize-rails' # https://github.com/markmcconachie/normalize-rails
gem 'omniauth-facebook' # https://github.com/mkdynamic/omniauth-facebook
gem 'rubocop', require: false # https://github.com/rubocop-hq/rubocop

group :development do
  gem 'web-console', '>= 3.3.0' # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
end

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw] # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'dotenv-rails' # https://github.com/bkeepers/dotenv
  gem 'rspec-rails', '~> 3.7'
end

group :development, :staging do
  gem 'rack-mini-profiler' # https://github.com/MiniProfiler/rack-mini-profiler
end

group :test do
  gem 'capybara', '>= 2.15', '< 4.0' # Adds support for Capybara system testing and selenium driver
  gem 'chromedriver-helper' # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'selenium-webdriver'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
